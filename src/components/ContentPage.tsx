import React from 'react'
import ReactMarkdown from 'react-markdown';

import { Content } from '../getContent';

export interface MarkdownRouteProps<Data = object> {
  markdown: Content<Data>;
}

export const ContentPage = (props: MarkdownRouteProps) => {
  return (
    <ReactMarkdown source={props.markdown.content}/>
  );
};
