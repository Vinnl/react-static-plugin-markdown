import React from 'react'
import renderer from 'react-test-renderer';

import { ContentPage } from './ContentPage';

it('should pass the content on to react-markdown', () => {
  const component = renderer.create(
    <ContentPage markdown={{ content: 'arbitrary content' } as any}/>
  );
  const tree = component.toJSON();

  expect(tree).toMatchSnapshot();
});