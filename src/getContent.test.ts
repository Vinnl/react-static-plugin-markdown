import { getContent } from './getContent';

let mockFiles: string[];
let mockContent: string;
jest.mock('fs', () => ({
  ...require.requireActual('fs'),
  readdir: jest.fn((_dir: any, callback: any) => callback(undefined, mockFiles)),
  readFile: jest.fn((_path: any, _options: any, callback: any) => callback(undefined, mockContent)),
  stat: jest.fn((_path: any, callback: any) => callback(undefined, { isFile: () => true })),
}));

beforeEach(() => {
  mockFiles = [ 'file1.md', 'file2.md' ];
  mockContent = 'Arbitrary content';
  const mockedFs = require.requireMock('fs');
  mockedFs.readdir.mockClear();
  mockedFs.readFile.mockClear();
});

it('should create items for every Markdown file', async () => {
  mockFiles = ['file1.md', 'file2.md']
  const content = await getContent();

  expect(content.length).toBe(2);
});

it('should ignore non-markdown files', async () => {
  mockFiles = ['file1.md', 'file2.txt', 'file3']
  const content = await getContent();

  expect(content.length).toBe(1);
});

it('should attach file names as metadata', async () => {
  mockFiles = ['file1.md']
  const content = await getContent();

  expect(content[0].filename).toBe('file1.md');
});

it('should attach top matter as metadata', async () => {
  mockContent = `---
some_data: some value
---
Some content`;
  const markdown = await getContent<{some_data: string}>();

  expect(markdown[0].data.some_data).toBe('some value');
  expect(markdown[0].content).toBe('Some content');
});

it('should be able to get content from a different directory', async () => {
  const mockedReaddir = require.requireMock('fs').readdir;

  await getContent('some-dir');

  expect(mockedReaddir.mock.calls.length).toBe(1);
  expect(mockedReaddir.mock.calls[0][0]).toBe('some-dir');
});
