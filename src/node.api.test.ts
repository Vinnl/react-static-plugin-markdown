const mockedGetContent = jest.fn(() => Promise.resolve([{
  content: 'Arbitrary content',
  filename: 'arbitrary_file.md',
  data: {},
}]));
jest.mock('./getContent', () => ({ getContent: mockedGetContent }));

import nodeApi from './node.api';

const mockReactStaticConfig = {
  getRoutes: () => [{
    path: '/',
    component: 'arbitrary/Component',
  }],
};

beforeEach(() => {
  mockedGetContent.mockClear();
});

it('should error out if no render component was provided', async () => {
  const originalConsoleError = console.error;
  const mockConsoleError = jest.fn();
  console.error = mockConsoleError;

  expect(nodeApi({} as any)).toBeUndefined();
  expect(mockConsoleError.mock.calls.length).toBe(1);
  expect(mockConsoleError.mock.calls[0][0])
    .toBe('[react-static-plugin-markdown] Please specify the path to the component that will render the markdown. Make sure to wrap that component in withRouteData().');

  console.error = originalConsoleError;
});

it('should preserve the original routes', async () => {
  const plugin = nodeApi({
    renderComponent: 'arbitrary/Component',
  })!;

  const mockConfig = {
    getRoutes: () => [ { path: 'some-path', component: 'some/Component' } ],
  };

  const newConfig = plugin.config(mockConfig);
  const newRoutes = await newConfig.getRoutes();

  expect(newRoutes[0]).toEqual({ path: 'some-path', component: 'some/Component' });
});

it('should preserve the original routes, even when they are wrapped in a promise', async () => {
  const plugin = nodeApi({
    renderComponent: 'arbitrary/Component',
  })!;

  const mockConfig = {
    getRoutes: () => Promise.resolve([ { path: 'some-path', component: 'some/Component' } ]),
  };

  const newConfig = plugin.config(mockConfig);
  const newRoutes = await newConfig.getRoutes();

  expect(newRoutes[0]).toEqual({ path: 'some-path', component: 'some/Component' });
});

it('should create new routes for every Markdown file', async () => {
  const plugin = nodeApi({
    renderComponent: 'some/Component',
  })!;

  mockedGetContent.mockReturnValueOnce(Promise.resolve([
    { filename: 'file1.md', content: 'Arbitrary content' },
    { filename: 'file2.md', content: 'Arbitrary content' },
  ]));

  const newConfig = plugin.config(mockReactStaticConfig);
  const newRoutes = await newConfig.getRoutes();

  expect(newRoutes[newRoutes.length - 2])
    .toMatchObject({ path: 'file1', component: 'some/Component' });
  expect(newRoutes[newRoutes.length - 1])
    .toMatchObject({ path: 'file2', component: 'some/Component' });
});

it('should pass the content and metadata to the render component', async () => {
  const plugin = nodeApi({
    renderComponent: 'arbitrary/Component',
  })!;

  mockedGetContent.mockReturnValueOnce(Promise.resolve([
    {
      filename: 'file.md',
      content: 'Some content',
      data: { some: 'metadata' },
    },
  ]));

  const newConfig = plugin.config(mockReactStaticConfig);
  const newRoutes = await newConfig.getRoutes();

  const routeData = (newRoutes[newRoutes.length - 1] as any).getData();
  expect(routeData).toEqual({ markdown: {
    content: 'Some content',
    data: { some: 'metadata' },
    filename: 'file.md',
  }});
});
