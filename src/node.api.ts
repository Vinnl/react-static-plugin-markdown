import * as React from 'react';
import { getContent } from './getContent';

interface Options {
  /**
   * Path to the component that will be used to render the Markdown
   */
  renderComponent: string;
  /**
   * Directory containing Markdown files
   */
  contentDir?: string;
};

interface StaticRoute {
  path: string;
  component: string;
  getData?: () => any;
};
interface StaticRedirectRoute {
  path: string;
  redirect: string;
}
type StaticRoutes = Array<StaticRoute | StaticRedirectRoute>;
interface ReactStaticConfig {
  getRoutes: () => StaticRoutes | Promise<StaticRoutes>;
};

export default (options: Options) => {
  if (typeof options.renderComponent === 'undefined') {
    console.error('[react-static-plugin-markdown] Please specify the path to the component that will render the markdown. Make sure to wrap that component in withRouteData().');
    return;
  }

  return {
    config: (config: ReactStaticConfig) => {
      const newConfig = {...config};
      newConfig.getRoutes = async () => {
        const originalRoutes = await config.getRoutes();
        const markdownRoutes: StaticRoutes =
          (await getContent(options.contentDir))
          .map((content) => ({
            path: content.filename.substring(0, content.filename.length - '.md'.length),
            component: options.renderComponent,
            getData: () => ({ markdown: content }),
          }));

        return originalRoutes.concat(markdownRoutes);
      };

      return newConfig;
    },
  };
};
