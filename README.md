react-static-plugin-markdown
======
A plugin for [React Static](https://react-static.js.org/) that parses Markdown files to pages in your React Static app.

## Installation

```bash
npm install react-static-plugin-markdown
```

## Usage

There are three steps to complete before being able to use this plugin.

First, create a component that will render the Markdown file's page. It can use the `ContentPage` component from this package to render the actual content, as long as you pass it the route data. At its bare minimum, the following would be sufficient:

```javascript
// src/containers/ContentPage
import React from 'react'
import { withRouteData } from 'react-static';
import { ContentPage } from 'react-static-plugin-markdown';

export default withRouteData(ContentPage);
```

Then, add the plugin to your `static.config.js`, passing the path to the above render component as an option:

```javascript
export default {
  plugins: [
    [
      'react-static-plugin-markdown',
      { renderComponent: 'src/containers/ContentPage' },
    ],
  ]
};
```

Finally, write your Markdown files in the `/content` directory. For example, `/content/some-page.md` will be visible on your website at the path `/some-page/`.

## Options

Options [can be passed by using an array](https://github.com/nozzle/react-static/tree/master/docs/plugins#plugin-options).

### `renderComponent: string`

Required.

Path to the component that will render the Markdown file. See [The render component](#the-render-component) for more details.

### `contentDir: string`

Default value: `content`

Folder in which your Markdown pages are located.

## The render component

Because plugins cannot access route data, you will manually have to pass that to the render component. At the bare minimum, it looks as follows:

```javascript
import React from 'react'
import { withRouteData } from 'react-static';
import { ContentPage } from 'react-static-plugin-markdown';

export default withRouteData(ContentPage);
```

However, this also provides you with the opportunity to integrate that Markdown page in additional HTML. Just make sure to pass the route data on to the `<ContentPage>` element:

```javascript
import React from 'react'
import { withRouteData } from 'react-static';
import { ContentPage } from 'react-static-plugin-markdown';

export default withRouteData((props) => (
  <article>
    <h1>{props.markdown.filename}</h1>
    <ContentPage {...props}/>
  </article>
));
```

You can also use this to replace the default [remark](https://remark.js.org/)-based Markdown renderer by a different renderer, simply by replacing the default `<ContentPage>` element with a different element:

```javascript
import React from 'react'
import { withRouteData } from 'react-static';
import ReactCommonmark from 'react-commonmark';

export default withRouteData((props) => (
  <ReactCommonmark source={props.markdown.content}/>
));
```

For a full list of the properties available to the render component, see [Route data](#route-data).

## Route data

The [render component](#the-render-component), when wrapped in [`withRouteData`](https://github.com/nozzle/react-static/blob/master/docs/components.md#routedata), receives a `markdown` prop that provides the following data:

### `content: string`

The original, unparsed Markdown.

### `filename: string`

The filename of the original Markdown file, e.g. `some-file.md`.

### `data: object`

Any YAML front matter included at the top of your Markdown file, parsed into a Javascript object by [gray-matter](https://www.npmjs.com/package/gray-matter). For example, given the following Markdown file:

```markdown
---
title: Computing Machinery and Intelligence
---
# The Imitation Game
I propose to consider the question, 'Can Machines Think?' This should...
```

Its data could be used by the render component as follows:

```javascript
import React from 'react'
import { withRouteData } from 'react-static';
import { ContentPage } from 'react-static-plugin-markdown';

export default withRouteData((props) => (
  <article>
    <h1>{props.markdown.data.title}</h1>
    <ContentPage {...props}/>
  </article>
));
```

## Changelog

See [`CHANGELOG.md`](https://gitlab.com/Vinnl/react-static-plugin-markdown/blob/master/CHANGELOG.md).

## License

MIT © [Vincent Tunru](https://vincenttunru.com)
