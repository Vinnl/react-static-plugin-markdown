# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2019-01-11

### New features

- Your Markdown files can now be located in subdirectories. That means that `/content/directory/page.md` will be visible on your website at the path `/directory/page/`.

## [1.0.0] - 2019-01-06

### New features

- First release!
